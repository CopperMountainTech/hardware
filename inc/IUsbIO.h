#ifndef IUSBIO_H
#define IUSBIO_H

#include <stdint.h>
#include <QDebug>

#include "HwGlobal.h"
#include "DeviceId.h"

namespace Planar {
namespace Hardware {

class HARDWARE_EXPORT  IUsbEp //: public QObject
{
    //  Q_OBJECT

public:
    virtual ~IUsbEp() {}
    virtual bool cancel() = 0;
    virtual bool waitCompleted() = 0;
};


class HARDWARE_EXPORT IUsbBulkEp : public IUsbEp
{
    //  Q_OBJECT
public:
    virtual ~IUsbBulkEp() {}

    virtual int EpNumber() = 0;
    virtual bool BeginXfer(void* buf, int length, int msTimeout = 0) = 0;
};


class HARDWARE_EXPORT IUsbControlEp : public IUsbEp
{
    //  Q_OBJECT
public:
    virtual ~IUsbControlEp() {}

    virtual bool beginXfer(uint8_t bmreqType, uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300) = 0;
    virtual bool xfer(uint8_t bmreqType, uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300) = 0;

    // helpers
    bool VenXferOut(uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300)
    {
        return xfer(0x40, bRequest, wIndex, wValue, wLength, buf, msTimeout);
    }

    bool VenXferIn(uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300)
    {
        return xfer(0xC0, bRequest, wIndex, wValue, wLength, buf, msTimeout);
    }
};


} // namespace Hardware
} // namespace Planar

#endif // IUSBIO_H
