#ifndef DEVICECONTAINER_H
#define DEVICECONTAINER_H

#include <QObject>
#include <QMap>

#include "HwGlobal.h"
#include "DevicePropertyBase.h"
#include "DeviceBase.h"
#include "DeviceId.h"

namespace Planar {
namespace Hardware {

class HARDWARE_EXPORT DevicePool : public QObject
{
public:
    DevicePool(const QJsonObject& jsonDeviceObj);
    ~DevicePool();

    bool isSupported(DeviceId deviceId);                // true, если поддерживается этим классом
    DeviceBase* checkAndAppend(DeviceBase* device);     // Вызывается диспетчером устройств при физическом подключении устройства
    bool remove(DeviceId id);                           // Вызывается диспетчером устройств при физическом отключении устройства
    DeviceBase* findDevice(DeviceId item);              // Возвращает устройство по USB идентификаторам
    DeviceBase* findDevice(QString serial);             // Возвращает устройство по серийному номеру
    DeviceBase* findDevice();                           // Возвращает первое устройство в пуле
    QString findKey(DeviceId item);
    int count()
    {
        return _devicePool.size();   // Кол-во устройст в пуле
    }
    virtual DeviceBase* createDevice(DeviceId id, Planar::AnalyzerItem* parent) = 0;  // Наследник должен реализовать этот метод
    QMap<QString, DeviceBase*> devices()                // устройства в этом пуле
    {
        return _devicePool;
    }

    // Свойства, одинаковые для всех приборов этой модели
    QString Type;                                         // тип устройства
    QString Model;                                        // название устройства
    QString Provider;                                     // Производитель
    quint16 VID;                                          // VendorId
    quint16 PID;                                          // ProductId

protected:
    QJsonObject _jsonDeviceObj;
    QMap<QString, DeviceBase*> _devicePool;
};

} // namespace Hardware
} // namespace Planar

#endif // DEVICECONTAINER_H
