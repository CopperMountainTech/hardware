#ifndef SHAREDMEMORY_H
#define SHAREDMEMORY_H


#include <QSharedMemory>
#include "DeviceId.h"


namespace Planar {
namespace Hardware {

#define MARKER_FREE  0x00
#define MARKER_INIT  0x01
#define LIST_SIZE 64


struct SHARED_ID {
    uint8_t Marker;                 // Признак инициализации
    bool Selected;                 // 0 - не выбран ни в одном процессе; 1 - кем то выбран
    DeviceId Id;
};


class SharedMemory : public QSharedMemory
{
public:
    SharedMemory(QString key);
    ~SharedMemory();

    bool CreateOrAttach();

    int Find(DeviceId id);
    bool Select(DeviceId id, bool
                select);                                  // изменение свойства Select в общей памяти
    DeviceId GetItem(int idx);
    bool Append(DeviceId id);


//    bool Busy(bool valueIfNotFound, uint8_t *key, int size);                // cвойство Busy из общей памяти






    bool Remove(DeviceId id);

protected:
//    SHARED_ID _vnaList[LIST_SIZE];

    SHARED_ID* _shId;

    int Find(uint8_t* key, int keySize);
    void UpdateItemEx(int idx, SHARED_ID item);

//   SHARED_ID GetItemEx(int idx);

};

} // namespace Hardware
} // namespace Planar
#endif // SHAREDMEMORY_H
