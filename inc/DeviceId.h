#ifndef USBDEVITEM_H
#define USBDEVITEM_H

#include <stdint.h>

namespace Planar {
namespace Hardware {

//
// Однозначно идентифицирует (USB?) устройство.
//
class DeviceId
{
public:
    DeviceId(int vid, int pid, int did): Bus(0), Address(0), Port(0), VID(vid), PID(pid), DID(did)
    {
        memset(Key, 0, sizeof(Key));
    }
    DeviceId(): Bus(0), Address(0), Port(0), VID(0), PID(0), DID(0)
    {
        memset(Key, 0, sizeof(Key));
    }
    uint8_t Bus;
    uint8_t Address;
    uint8_t Port;
    uint16_t VID;
    uint16_t PID;
    uint16_t DID;
    uint8_t Key[8];            // Serial

    bool operator == ( const DeviceId& item )
    {
        if ((VID == item.VID) &&
                (PID == item.PID) &&
                (DID == item.DID) &&
                (Bus == item.Bus) &&
                (Port == item.Port)
                //(VnaId.Address == item.VnaId.Address)
           )
            return true;
        else
            return false;
    }
};
} // namespace Hardware
} // namespace Planar
#endif // USBDEVITEM_H
