#ifndef IPLUGIN_H
#define IPLUGIN_H

#include <QJsonObject>

#include "DevicePool.h"
#include "Properties/ComponentInfo.h"

namespace Planar {
namespace Hardware {

class IPlugin
{
public:
    virtual ~IPlugin() {}
    virtual DevicePool* Instance(const QJsonObject& jsonDeviceObj) = 0;
    virtual void appendProperties(QList<ComponentInfo>* /*properties*/) {}
};
} // namespace Hardware
} // namespace Planar

Q_DECLARE_INTERFACE(Planar::Hardware::IPlugin, "PLANAR.HW.Common.IPlugin")

#endif // IPLAGIN_H
