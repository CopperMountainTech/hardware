#ifndef NOTIFYWINDOW_H
#define NOTIFYWINDOW_H

#include "DeviceManager.h"

namespace Planar {
namespace Hardware {

typedef void (DeviceManager::*callbackConnectionChangedType)();

bool HotPlugNotifyCreate(DeviceManager* deviceManager, callbackConnectionChangedType connectionChanged);
void HotPlugNotifyDestroy();

} // namespace Hardware
} // namespace Planar
#endif // NOTIFYWINDOW_H
