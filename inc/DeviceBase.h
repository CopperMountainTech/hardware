#ifndef DEVICEBASE_H
#define DEVICEBASE_H

#include <QObject>

#include "HwGlobal.h"
#include "DeviceId.h"
#include "IUsbIO.h"
#include "AnalyzerItem.h"

namespace Planar {
namespace Hardware {

enum STATE_NOTIFY
{
    IDLE,
    MCU_FW_LOAD,
    SELECTED_CHANGED
};

class DevicePool;
//
// Базовый класс устройств (USB).
//
class HARDWARE_EXPORT DeviceBase : public Planar::AnalyzerItem
{
    Q_OBJECT
public:
    DeviceBase(DevicePool* pool, Planar::AnalyzerItem* parent) :
        Planar::AnalyzerItem(parent),
        _pool(pool),
        _selected(false),
        _firstSelect(true),
        _connected(false)
    {
    }

    virtual ~DeviceBase() override {}

    virtual bool initialize(bool firstInstance) = 0;                    // вызвать первым. true для рабочих прошивок

    virtual DeviceId uniqueId() = 0;                                    // уникальный идентификатор устройства

    virtual QString serial()
    {
        return _serial;                                                 // Возвращает серийный номер устройства
    }

    virtual bool selected()
    {
        return _selected;                                               // true, если устройство выбрано в текущем процессе
    }

    virtual bool connected()
    {
        return _connected;                                              // true, если устройство подключено, false, если выбрано и отключено
    }

    virtual void setConnected(bool connected)                                   // вызвать после изменения состояния подключения
    {
        _connected = connected;
        if (connected)
        {
            if (_selected)
                activate();
        }
        else
        {
            deactivate();
        }
    }

    virtual bool select(bool select)                                    // Выбора у-ва. true, если успешно изменён.
    {
        if (select)
        {
            if (!_selected)
                _selected = activate();
        }
        else
        {
            if (_selected)
            {
                deactivate();
                _selected = false;
            }
        }

        return _selected == select;                                     // успешно изменён
    }

    virtual bool isSame(DeviceBase*) = 0;                               // true, если этот класс обслуживает то же прибор

    DevicePool* pool()
    {
        return _pool;                                                   // класс группы устройств
    }

    virtual IUsbControlEp* controlPipe() = 0;                             // Интерфейс ControlEp

protected:
    DevicePool* _pool;                                                  // pool устройств этого типа.
    bool _selected;
    bool _firstSelect;
    bool _connected;

    QString _serial;

    virtual bool activate()
    {
        return true;   // Здесь должны создаваться все компоненты устройства
    }

    virtual void deactivate() {}

    virtual bool open()
    {
        return true;
    }

    virtual void close() {}

private:

signals:
    void StateNotify(int state);
    // signals from property
    void propertyChanged();
};

} // namespace Hardware
} // namespace Planar

#endif // DEVICEBASE_H
