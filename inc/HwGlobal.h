#ifndef HW_GLOBAL_H
#define HW_GLOBAL_H

#include <QtCore/qglobal.h>

#ifdef 		HARDWARE_LIB
#  define 	HARDWARE_EXPORT Q_DECL_EXPORT
#else
# define 	HARDWARE_EXPORT Q_DECL_IMPORT
#endif


#endif // HW_GLOBAL_H
