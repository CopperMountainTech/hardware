#pragma once

#include <QObject>
#include <QVector>
#include <QMutex>
#include <QWaitCondition>
#include <atomic>

#include "HwGlobal.h"

#include "libusb.h"
#include "IUsbIO.h"

namespace Planar {
namespace Hardware {

//
// Ожидание завершения для LibUsb Xfer
//
class HwWaitCondition
{
public:
    HwWaitCondition(int poolSize = 1);
    ~HwWaitCondition();

    void  waitCompleted(unsigned long msTimeout = ULONG_MAX); // Ждёт пока хотя бы один объект в пуле завершит работу
    void  addCompletedPart();                               // Увеличивает счётчик объектов, завершивших работу и будит ожидающий поток
    bool  waitFree(unsigned long msTimeout = ULONG_MAX);      // Ждёт пока в пуле не появится хотя бы один свободный объект
    bool  waitAll(unsigned long msTimeout = ULONG_MAX);       // Ждёт, пока в пуле все объекты завершат работу
    void  reset();                                          // сброс счётчиков в исходное состояние
    void  acquireFreePart();
    void  releaseFreePart();
    bool  checkCompletedAll();                              // true, если все транзакции завершены


protected:
    QMutex _mutex;
    QWaitCondition _waitCondition;
    int _countCompleted;                    // счётчик объектов, завершивших обработку
    int _countFree;                         // счётчик свободных объектов в пуле
    int _poolSize;

    void releaseCompleted();
};


//
// poolXfer (базовый класс)
//
class HARDWARE_EXPORT LibUsbPoolXfer
{
public:
    LibUsbPoolXfer(libusb_device_handle* hDev, unsigned char epNum, int poolSize, unsigned int timeout);
    ~LibUsbPoolXfer();

    bool waitCompleted(unsigned char** buf = nullptr, int* actualLength = nullptr);// ждёт завершения передачи. Используется для синхронизации IN pipe
    bool waitFree(unsigned long msTimeout = ULONG_MAX); // ждёт свободного буфера для в пуле. Используется для синхронизации OUT pipe)

    void cancel();

    bool waitCompletedAll();
    void reset();

protected:
    libusb_device_handle* _hDev;
    unsigned char _epNum;
    unsigned int _msTimeout;
    QList<libusb_transfer*> _pool;
    int _poolIdxCompleted;
    int _poolIdxFree;

    HwWaitCondition _syncCondition;

    bool submit(libusb_transfer* transfer);
};

//
// poolXferIn
//
class HARDWARE_EXPORT LibUsbPoolXferBuffered : public LibUsbPoolXfer
{
public:
    LibUsbPoolXferBuffered(libusb_device_handle* hDev, unsigned char epNum, int poolSize, int pktSize, unsigned int timeout);
    ~LibUsbPoolXferBuffered();

    bool beginXferIn();
    bool beginXferOut(char* buf, int length);

protected:
    int _pktSize;
};


//
// poolXferOut
//
class HARDWARE_EXPORT LibUsbPoolXferDirect : public LibUsbPoolXfer
{
public:
    LibUsbPoolXferDirect(libusb_device_handle* hDev, unsigned char epNum, int poolSize, unsigned int timeout);
    ~LibUsbPoolXferDirect();

    bool beginXfer(void* buf, int length);
};



class HARDWARE_EXPORT LibUsbControlXfer : public IUsbControlEp
{
public:
    LibUsbControlXfer(libusb_device_handle* hDev);
    ~LibUsbControlXfer();

// IUsbControlEp override
    bool waitCompleted();
    bool cancel();
    bool beginXfer(uint8_t bmreqType, uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300);
    bool xfer(uint8_t bmreqType, uint8_t bRequest, uint16_t wIndex, uint16_t wValue, uint16_t wLength = 0, void* buf = nullptr, uint32_t msTimeout = 300);

protected:
    libusb_device_handle* _hDev;
    unsigned char* _xferBuf;
    void* _userBuf;
    libusb_transfer* _transfer;
    HwWaitCondition* _completed;
};



//
// Получение Handle устройства и другой инфы от LibUsb
//
class HARDWARE_EXPORT LibUsbDevice
{
public:
    static libusb_device_handle* Open(DeviceId id);                 // Handle
    static void Close(libusb_device_handle* hdev);                  // Close device
    static int GetEpSize(libusb_device_handle* hDev, unsigned char epNum);    // Размер EP
};


} // namespace Hardware
} // namespace Planar

