#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <QObject>
#include <QMap>
#include <vector>
#include <QThread>
#include <QList>

#include "HwGlobal.h"
#include "DeviceBase.h"
#include "DeviceId.h"
#include "DevicePool.h"
#include "AnalyzerItem.h"
#include "IPlugin.h"

#ifdef MPROC
#include "SharedMemory.h"
#endif

#include "LibUsbEvThread.h"

namespace Planar {
namespace Hardware {

class HARDWARE_EXPORT DeviceManager : public Planar::AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    DeviceManager(Planar::AnalyzerItem* parent);
    ~DeviceManager() override;

    bool isModelSupported(QString model);
    int deviceCount(QString model);                          // Кол-во устройств этой модели в пуле (подключенных)
    DeviceBase* deviceInstance(QString model, QString serial);// по серийному
    DeviceBase* deviceInstance(QString model);                // первый, заданной модели
    DeviceBase* deviceInstance();                             // первый попавшийся

    QMap<QString, DevicePool*> devices()
    {
        return _devices;   // Все ус-ва
    }
    QList<QString> supportedModels();                        // Список поддерживаемых моделей
    bool select(DeviceBase* device, bool select);            // Выбор ус-ва. Lock для других процессов.

    void pluginInstance(QList<ComponentInfo>* properties);

    void hotPlugInitialize();

protected:

#ifdef MPROC
    SharedMemory* _sharedMemory;
#endif

    int indexOf(DeviceId item, std::vector<DeviceId>& items);
    bool contains(DeviceId item, std::vector<DeviceId>& items);
    DevicePool* findPool(DeviceId item);

    void callbackHotPlugNotify();

    void onConnect(DeviceId id);
    void disconnect(DeviceId id);
    bool updateUsbDeviceList();

    QMap<QString, DevicePool*> _devices;                     // Группируются по типу.
    std::vector<DeviceId> _usbDevices;                       // список USB устройств

    bool _initialized = false;
    LibUsbEvThread* _libUsbEvThread;
    QThread* _evThread;

private:
    static DeviceManager* _instance;

protected slots:
    void slotHotPlugNotify();
signals:
    void hotPlugNotify();
    void connectionChanged(DevicePool* pool, DeviceBase* device, bool isConnected);
    void deviceListChanged(DevicePool* pool, DeviceBase* device, bool isAppend);    // после изменения коллекции
};

} // namespace Hardware
} // namespace Planar

#endif
