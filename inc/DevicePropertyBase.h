#ifndef DEVICEPROPERTYBASE_H
#define DEVICEPROPERTYBASE_H

#include <QJsonObject>

#include "Units.h"


namespace Planar {
namespace Hardware {

// базовые ключи JSON файла
#define JSON_SECTION_MODEL         "MODEL"

#define JSON_KEY_TYPE               "TYPE"              // VNA, GEN, SA...
#define JSON_KEY_PROVIDER           "PROVIDER"
#define JSON_KEY_NAME               "NAME"
#define JSON_KEY_VID                "VID"
#define JSON_KEY_PID                "PID"


//#define JSON_SECTION_USB            "USB"

#define JSON_KEY_DID                "DID"
#define JSON_KEY_OUT_EP             "OUT_EP"
#define JSON_KEY_IN_EP              "IN_EP"


//
//
//
class  DevicePropertyBase
{
public:
    DevicePropertyBase() : JsonDeviceObj(), PluginName(""), Type(""), Model(""), IsLoader(false),
        Provider("PLANAR LLC") {}
    ~DevicePropertyBase() {}

    void Init(const QJsonObject& jsonDeviceObj, QString pluginName = "")
    {
        JsonDeviceObj = jsonDeviceObj;
        PluginName = pluginName;
        QJsonObject json = jsonDeviceObj[JSON_SECTION_MODEL].toObject();
        Type = json[JSON_KEY_TYPE].toString("");
        Model = json[JSON_KEY_NAME].toString("");
        // Provider = json[JSON_KEY_PROVIDER].toString("PLANAR LLC");
        //IsLoader = json[JSON_KEY_LOADER].toBool(false);

        //   json = jsonDeviceObj[JSON_SECTION_USB].toObject();
        VID = json[JSON_KEY_VID].toInt();
        PID = json[JSON_KEY_PID].toInt();
        DID = json[JSON_KEY_DID].toInt();
        OutEP = json[JSON_KEY_OUT_EP].toInt();
        InEP = json[JSON_KEY_IN_EP].toInt();

    }
    QJsonObject JsonDeviceObj;
    QString PluginName;
    QString Type;                                         // тип устройства
    QString Model;                                        // название устройства
    bool IsLoader;                                        // загрузчик
    QString Provider;                                     // Производитель
    quint16 VID;                                          // VendorId
    quint16 PID;                                          // ProductId
    quint16 DID;                                          // DeviceId
    quint16 OutEP;                                        // Номер EP OUT
    quint16 InEP;                                         // Номер EP IN
};

} // namespace Hardware
} // namespace Planar

#endif // DEVICEPROPERTYBASE_H
