#ifndef LIBUSBEVTHREAD_H
#define LIBUSBEVTHREAD_H

#include <atomic>

#include <QObject>
#include <QWaitCondition>
#include <stdint.h>



namespace Planar {
namespace Hardware {

class LibUsbEvThread : public QObject
{
    Q_OBJECT

public:
    LibUsbEvThread();
    ~LibUsbEvThread();

    void Cancel();                 // требование завершить работу метода DoWork

public slots:
    void DoWork();                  // Метод, выполняющийся в другом потоке

//signals:
//    void XferCompleted(unsigned char ep, XferContext *chunk);

protected:
    std::atomic<bool> _cancel;
};

} // namespace Hardware
} // namespace Planar



#endif // LIBUSBEVTHREAD_H
